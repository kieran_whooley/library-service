package edu.ait.libraryservice.controllers;

import edu.ait.libraryservice.dto.Library;
import edu.ait.libraryservice.exceptions.LibraryNotFoundException;
import edu.ait.libraryservice.repositories.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class LibraryController {

    @Autowired
    LibraryRepository libraryRepository;

    @GetMapping("libraries")
    public List<Library> getAllLibraries() {
        return libraryRepository.findAll();
    }

    @GetMapping("libraries/{libraryId}")
    public Library getLibraryById(@PathVariable int libraryId) {

        Optional<Library> foundLibrary = libraryRepository.findById(libraryId);
        if(foundLibrary.isPresent())
            return foundLibrary.get();
        else
            throw new LibraryNotFoundException("The library ID entered cannot be found in the database");
    }

    @GetMapping("libraries/sector/{sector}")
    public List<Library> getAllLibrariesBySector(@PathVariable String sector) {
        return libraryRepository.findBySector(sector);
    }

    @GetMapping("libraries/startswith/{letter}")
    public List<Library> getAllLibrariesStartingWithX(@PathVariable String letter) {
        try {
            return libraryRepository.findByNameStartingWith(letter.toUpperCase());
        }
        catch (EmptyResultDataAccessException e) {
            throw new LibraryNotFoundException("There are no libraries beginning with the letter: " + letter.toUpperCase());
        }
    }

    @PostMapping("libraries/")
    public ResponseEntity createLibrary(@Valid @RequestBody Library newLibrary) {
        Library savedLibrary =libraryRepository.save(newLibrary);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("{id}")
                .buildAndExpand(newLibrary.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("libraries/")
    public ResponseEntity updateLibrary(@RequestBody Library newLibrary) {
        if (newLibrary.getId() != null){
            libraryRepository.save(newLibrary);
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        else {
            Library savedLibrary = libraryRepository.save(newLibrary);

            URI location =ServletUriComponentsBuilder
                    .fromCurrentRequest().path("{id}")
                    .buildAndExpand(savedLibrary.getId()).toUri();
            return ResponseEntity.created(location).build();
        }
    }

    @DeleteMapping("libraries")
    public void deleteAllLibraries() {
        try {
            libraryRepository.deleteAll();
        }
        catch (EmptyResultDataAccessException e) {
            throw new LibraryNotFoundException("No libraries in database available for deletion");
        }

    }

    @DeleteMapping("libraries/{libraryId}")
    public void deleteIndividualLibrary(@PathVariable int libraryId) {
        try {
            libraryRepository.deleteById(libraryId);
        }
        catch (EmptyResultDataAccessException e) {
            throw new LibraryNotFoundException("Unable to delete library with id: " + libraryId);
        }
    }

}
