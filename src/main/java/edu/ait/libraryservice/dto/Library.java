package edu.ait.libraryservice.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Entity
public class Library {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String location;
    private String sector;
    @Size(min = 7, max = 7, message = "Eircode must be 7 characters in length")
    private String eircode;
    @Email(message =  "Invalid email entered")
    private String email;

    public Library() {

    }

    public Library(Integer id, String name, String location, String sector, String eircode, String email) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.sector = sector;
        this.eircode = eircode;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getEircode() {
        return eircode;
    }

    public void setEircode(String eircode) {
        this.eircode = eircode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Library{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", sector='" + sector + '\'' +
                ", eircode='" + eircode + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
