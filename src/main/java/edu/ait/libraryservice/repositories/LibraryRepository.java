package edu.ait.libraryservice.repositories;

import edu.ait.libraryservice.dto.Library;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LibraryRepository extends JpaRepository<Library, Integer> {

    List<Library> findBySector(String sector);
    List<Library> findByNameStartingWith(String letter);

}

