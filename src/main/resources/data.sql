insert into library (name, location, sector, eircode, email) values ('Ballincollig Library', 'The Village Centre', 'West', 'P31H674', 'ballincollig_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Bishopstown Library', 'Wilton', 'West', 'T12RR84', 'bishopstown_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Blackpool Library', 'Redforge Road', 'North', 'T23AP11', 'blackpool_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Blarney Library', 'The Square', 'North', 'T23AC97', 'blarney_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('City Library', '57-61 Grand Parade', 'Centre', 'T12NT99', 'libraries@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Douglas Library', 'Douglas Village Shopping Centre', 'South', 'T12HDY2', 'douglas_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Frank O Connor Library', 'Mayfield', 'North', 'T23E651', 'mayfield_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Glanmire Library', 'Hazelwood Shopping Centre', 'East', 'T45E033', 'glanmire_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Hollyhill Library', 'Harbour View Road', 'North', 'T23N250', 'hollyhill_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Tory Top Library', 'Ballyphehane', 'South', 'T12WP57', 'torytop_library@corkcity.ie');
insert into library (name, location, sector, eircode, email) values ('Boole Library', 'UCC', 'Centre', 'T12ND89', 'library@ucc.ie');
insert into library (name, location, sector, eircode, email) values ('CIT - MTU Library', 'Bishopstown', 'West', 'TE3NT99', 'library.info@cit.ie');
insert into library (name, location, sector, eircode, email) values ('Cork School of Music Library', 'Union Quay', 'Centre', 'TE3FE99', 'csm.library@cit.ie');
insert into library (name, location, sector, eircode, email) values ('National Maritime College of Ireland Library', 'Ringaskiddy', 'South', 'AH3NT67', 'maritime@nmci.ie');
insert into library (name, location, sector, eircode, email) values ('Youghal Library', 'North Main Street', 'East', 'P36FF99', 'youghal.library@corkcoco.ie');
insert into library (name, location, sector, eircode, email) values ('Fermoy Library', 'Connolly Street', 'North', 'P61C663', 'fermoy.library@corkcoco.ie')